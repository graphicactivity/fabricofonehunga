window.onload = function() {
  document.getElementById('page-body').className = 'loaded';
};

$(document).ready(function(){
  $('a.galleryBox').featherlightGallery({
    openSpeed: 300,
    closeOnClick: 'background',
    afterContent: function() {
      this.$legend = this.$legend || $('<div class="caption col-2"/>').insertAfter(this.$content);
      this.$legend.text(this.$currentTarget.attr('alt'));
    }
  });
});

(function($) { // Begin jQuery
  $(function() { // DOM ready
    // Toggle open and close nav styles on click
    $('#nav-toggle').click(function() {
      $('.trigger').slideToggle();
    });
    // Hamburger to X toggle
    $('#nav-toggle').on('click', function() {
      this.classList.toggle('active');
    });
  }); // end DOM ready
})(jQuery); // end jQuery

$(window).scroll(function() {
    var pageH = $('#right').height() - $(this).height();
    var pageT = this.scrollY - $('#right').offset().top;

    $('#scroll').scrollTop(pageT / pageH * ($('#left').height() - $(this).height()));
});

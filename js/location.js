L.mapbox.accessToken = 'pk.eyJ1IjoiYXNmZXJlbnMiLCJhIjoiUFVpUWtBSSJ9.BkWDrqIrFstKBHAKv9TsvQ';
var map = L.mapbox.map("map")
    .setView([-36.922, 174.789], 15);

var myLayer = L.mapbox.featureLayer().addTo(map);

var styleLayer = L.mapbox.styleLayer('mapbox://styles/asferens/citghtf2t000k2jplv49j76vr')
    .addTo(map);

var geoJson = [{
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7902,-36.9237254]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-fabric.svg",
            "iconSize": [16, 16], // size of the icon
            "iconAnchor": [16, 16], // point of the icon which will correspond to marker's location
            "popupAnchor": [0, -16], // point from which the popup should open relative to the iconAnchor
            "className": "dot"
        }
    }
}, {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.78459,-36.91596]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-1.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
}, /*{
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7834688,-36.90852]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-1.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},*/  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.8014,-36.9197]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-2.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7979,-36.9274501]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-3.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7863,-36.926]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-4.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},   {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7862,-36.9211494]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-5.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.78526,-36.9209]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-6.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7834,-36.9226995]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-7.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.785,-36.9258759]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-8.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7807,-36.91760]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-9.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7754,-36.9242]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-10.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7773,-36.9213]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-11.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
}, {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.770188,-36.9184]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-12.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.77368,-36.9152]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-13.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7766,-36.9117]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-14.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7830,-36.9050341]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-15.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7632,-36.9144876]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-16.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7789855,-36.9258839]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-17.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.764,-36.9291]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-18.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.776,-36.9284]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-19.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.8127,-36.91798]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-20.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7834455,-36.9200621]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-21.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7882987,-36.9227636]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-22.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7844626,-36.9235702]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-23.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7999477,-36.9226601]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-24.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7836778,-36.9307736]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-25.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7769236,-36.9277332]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-26.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.791,-36.9405546]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-27.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7767933,-36.905935]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-28.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
},  {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [174.7905667,-36.9207154]
    },
    "properties": {
        "icon": {
            "iconUrl": "/img/pin-29.svg",
            "iconSize": [16, 16],
            "iconAnchor": [16, 16],
            "popupAnchor": [0, -16],
            "className": "dot"
        }
    }
}];

// Set a custom icon on each marker based on feature properties.
myLayer.on('layeradd', function(e) {
    var marker = e.layer,
        feature = marker.feature;

    marker.setIcon(L.icon(feature.properties.icon));
});

// Add features to the map.
map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
myLayer.setGeoJSON(geoJson);
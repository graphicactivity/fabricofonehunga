---
layout: page
title: Updates & Invitations
permalink: /updates/
custom: Check back here for regular updates on the progress of Fabric of Onehunga, and exclusive invitations to preview.<br><br>For all of this information and more, follow us on <a href="https://www.facebook.com/fabricofonehunga/" target="_blank" rel="noopener noreferrer" class="social">Facebook</a>.
img: /img/updates-hero.jpg
pagecolor: "#bdbdbd"
titletwo: Updates &amp;<br> Invitations
more: Read the<br> Latest Updates
---

<div class="wrapper updates">
	<div id="scroll">
		<div id="left" class="left">
			<div class="first-space"></div>
			<div class="leftIn">

				<div class="top-space col-6">
					<div class="space-left column col-3">
						<img src="/img/here.png" alt="Here" title="Here">
					</div>
					<div class="space-left column col-3"><br>
					</div>
				</div>

				<div class="space col-6">
					<div class="space-right column col-3">
						<img src="/img/soon.png" alt="Soon" title="Soon">
					</div>
					<div class="space-left column col-3"><br>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="update-gallery">
		<a href="/img/considered-apartment-living.jpg" class="galleryBox" alt="Victoria Street Entrance"><img src="/img/considered-apartment-living.jpg" class="col-6 bottom-pad"></a>
		<a href="/img/outside-in.jpg" class="galleryBox" alt="Open living areas have been designed to naturally extend outdoors with sheltered balconies and landscaped ground-floor courtyards. Relax with a coffee and a good book in your private garden retreat."><img src="/img/outside-in.jpg"></a>
		<a href="/img/pedestrian-friendly.jpg" class="galleryBox" alt="Generously-sized glazed atriums continue the refined industrial aesthetic with exposed concrete and steel structures. Softened by extensive planting, these spaces create a memorable entry to the individual apartments."><img src="/img/pedestrian-friendly.jpg"></a>
		<a href="/img/pocket-park.jpg" class="galleryBox" alt="The common pocket park is a central focus and the social heart of FABRIC, encouraging residents to gather together, kick a ball or relax in the sun."><img src="/img/pocket-park.jpg" class="bottom-pad"></a>
		<a href="/img/well-connected.jpg" class="galleryBox" alt="Walk 500 metres to ONEHUNGA  Mall and the Train Station — close to  everything you need."><img src="/img/well-connected.jpg"></a>
		<a href="/img/foreshore.jpg" class="galleryBox" alt="Just a bike ride away from ONEHUNGA’s newly restored foreshore."><img src="/img/foreshore.jpg"></a>
		<a href="/img/designed-to-suit.jpg" class="galleryBox" alt="Living, Kitchen and Bedroom — Light Neutral Palette"><img src="/img/designed-to-suit.jpg" class="col-6 bottom-pad"></a>
		<a href="/img/light-neutral-landscape.jpg" class="galleryBox" alt="Light Neutral Palette"><img src="/img/light-neutral-landscape.jpg"></a>
		<a href="/img/fabric-bathroom.jpg" class="galleryBox" alt="Two Bed, Two Bath Apartment — Bathroom Detail (Display Apartment) "><img src="/img/fabric-bathroom.jpg"></a>
		<a href="/img/flexible-spaces.jpg" class="galleryBox" alt="Bedroom — Light Neutral Palette"><img src="/img/flexible-spaces.jpg"></a>
		<a href="/img/bay-reserve.jpg" class="galleryBox" alt="ONEHUNGA Bay Reserve is a dog-lover’s dream with plenty of space to run and play, just a leisurely 15-minute walk from your pet-friendly home at FABRIC."><img src="/img/bay-reserve.jpg"></a>
		<a href="/img/out-and-about.jpg" class="galleryBox" alt="The perfect place to get out and about, a morning seaside run or a weekend cycle is a familiar sight in ONEHUNGA."><img src="/img/out-and-about.jpg"></a>
		<a href="/img/village-views.jpg" class="galleryBox" alt="Living and Dining — Dark Industrial Palette"><img src="/img/village-views.jpg"></a>
		<a href="/img/one-bedroom.jpg" class="galleryBox" alt="One Bedroom Apartment, Living and Bedroom — Dark Industrial Palette"><img src="/img/one-bedroom.jpg"></a>
	</div>

	<div id="right" class="right">
	    <div class="rightIn">
			<ul class="post-list">
				{% for post in site.posts %}
				  <li>
				  	<img src="{{ site.baseurl }}/img/{{ post.image }}">
				  	<div class="space-left column col-2">
				    	<h4 class="date">{{ post.date | date: "%d/%m/%Y" }}</h4>
				    	<h3>{{ post.title }}</h3>
				    </div>
				    <div class="space-right column col-4">
				    	{{ post.excerpt }}
				    	<h4>{{ post.link }}</h4>
				    </div>
				  </li>
				{% endfor %}
			</ul>
		</div>
	</div>

</div>
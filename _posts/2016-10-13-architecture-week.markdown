---
layout: post
title: "ARCHITECTURE WEEK: HOUSING THE FUTURE"
date: 2016-10-13 13:05:16 +1200
categories: update fabric
image: architecture-week.jpg
link: <a href="https://www.facebook.com/fabricofonehunga/photos/a.1681300488858867.1073741828.1659220324400217/1693148367674079/?type=3&theater" target="_blank" rel="noopener noreferrer">Visit Link</a>
---

<p>FABRIC was recently included in  The New Zealand Institute of Architects 'New City Architecture' exhibition, part of Architecture Week 2016. The theme for this year was 'Housing the Future', and it’s exciting to be considered among the country’s best examples of successful city and urban living.</p>
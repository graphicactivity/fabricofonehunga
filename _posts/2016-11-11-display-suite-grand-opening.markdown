---
layout: post
title: "DISPLAY SUITE GRAND OPENING"
date: 2016-11-11 09:30:00 +1200
categories: update fabric
image: grand-opening.jpg
---

<p>View the fully furnished two bedroom display apartment styled by Homestyle at FABRIC of ONEHUNGA’s display suite  this weekend.</p><p>Grand opening 10:00AM – 4:00PM, Saturday 12 November &amp; Sunday  13 November. Open 11:00AM – 3:00PM seven days from Monday 14th November.</p>
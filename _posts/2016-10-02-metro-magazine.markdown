---
layout: post
title: "METRO MAGAZINE: A BRIGHT NEW FABRIC"
date: 2016-10-02 13:05:16 +1200
categories: update fabric
image: metro-magazine.jpg
link: <a href="/pdf/metro.pdf" target="_blank" rel="noopener noreferrer">As Seen in Metro</a>
---

<p>FABRIC features in this month's issue of Metro Magazine. Pick up your copy today and read more about how we’re playing a part in a wider plan for growth throughout Onehunga, ‘bringing new life to one of Auckland’s oldest character suburbs’.</p>

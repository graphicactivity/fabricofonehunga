---
layout: post
title: "Exclusive Preview"
date: 2016-10-18 13:05:16 +1200
categories: update fabric
image: exclusive-preview.jpg
link: <a href="/#register-link">Register now</a>
---

<p>Register now to experience FABRIC’s well-considered display apartment before the public opening. Call <a href="tel:0800258358" class="tel-phone">0800 258 358</a> or register online today.</p>

---
layout: post
title: "FABRIC <br> WELCOMES 2017"
date: 2017-02-09 02:30:00 +1200
categories: update fabric
image: twenty-seventeen.jpg
link: <a href="http://www.homestyle.co.nz/park-life/" target="_blank" rel="noopener noreferrer">Read Online</a>
---

<p>Happy New Year from the team at FABRIC of ONEHUNGA. Hope you enjoyed a restful Christmas break and that 2017 is off to a great start.</p><p>Sales are going really well at FABRIC and only two SHA apartments for eligible first home buyers remain from the first release!</p><p>We received confirmation from the Auckland City Council of our Resource Consent being granted and are also anticipating the new release of the second building at FABRIC next month.</p><p> We thought you might be interested to read the great feature on FABRIC in the latest issue of Homestyle Magazine, styled by editor Alice Lines.</p><p>Feel free to pick up a complimentary copy of the latest Homestyle Magazine from the display suite, which is now open for 2017 from 11AM—3PM daily. We’d love to show you through for another look at the display and to answer any questions you may have over a cup of coffee in our sunny courtyard!</p>
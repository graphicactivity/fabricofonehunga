---
layout: post
title: "FINAL BUILDING <br> IN STAGE ONE NOW <br> SELLING"
date: 2017-07-05 02:30:00 +1200
categories: update fabric
image: final-building-stage-one.jpg
---

<p>The third and final building of FABRIC’s first stage is now selling. Ashton Mitchell Architects have incorporated market feedback into the new, improved building design.</p><p>This building contains some exciting new layouts for two bedroom-one bathroom and two bedroom-two bathroom apartments. View plans and pricing at the display suite and take a tour of our full two-bedroom display apartment styled by Homestyle.</p>

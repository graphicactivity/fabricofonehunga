---
layout: post
title: "APARTMENT <br> SALES UPDATE"
date: 2016-12-14 09:30:00 +1200
categories: update fabric
image: sales-update.jpg
---

<p>We are delighted to announce that  the first release of FABRIC of Onehunga  is almost half sold.</p><p>The 2 bedroom, 1 bathroom  apartments are now sold out. There  are still great options remaining with  two 1 bedroom apartments left,  and a number of 2 bedroom 2 bathroom apartments available.</p><p>There’s still time to visit our  fully furnished two bedroom display apartment before the holiday season.  We are open daily 11:00AM – 3:00PM  and will be closed from the 24th December. We will reopen from the  16th January, 11:00AM – 3:00PM daily.</p><p>For an appointment outside of  open hours, register online here or  call 0800 258 358.</p>
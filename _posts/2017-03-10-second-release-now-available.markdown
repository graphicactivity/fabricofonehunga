---
layout: post
title: "SECOND RELEASE <br> NOW AVAILABLE"
date: 2017-03-10 02:30:00 +1200
categories: update fabric
image: open-architecture.jpg
---

<p>FABRIC’s first release has been immensely popular with first home buyers, Onehunga locals and those transitioning from larger suburban homes. With sales progressing well, a second release of 39 one, two and three bedroom apartments has been announced.</p><p>Visit the display suite to view plans and pricing on the new offering and take a tour of our full two bedroom display apartment styled by Homestyle editor, Alice Lines. Make your own homestyle at FABRIC.</p>